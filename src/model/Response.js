/**
 * dyplca-services-api
 * API for DyPLCA Services
 *
 * OpenAPI spec version: 1.0.0
 * Contact: yoann.pigne@univ-lehavre.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.DyplcaServicesApi) {
      root.DyplcaServicesApi = {};
    }
    root.DyplcaServicesApi.Response = factory(root.DyplcaServicesApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The Response model module.
   * @module model/Response
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>Response</code>.
   * @alias module:model/Response
   * @class
   */
  var exports = function() {
    var _this = this;



  };

  /**
   * Constructs a <code>Response</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Response} obj Optional instance to populate.
   * @return {module:model/Response} The populated <code>Response</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('ok')) {
        obj['ok'] = ApiClient.convertToType(data['ok'], 'Boolean');
      }
      if (data.hasOwnProperty('message')) {
        obj['message'] = ApiClient.convertToType(data['message'], 'String');
      }
    }
    return obj;
  }

  /**
   * was the requests successfull?
   * @member {Boolean} ok
   */
  exports.prototype['ok'] = undefined;
  /**
   * iformation about the response weither or not it went well.
   * @member {String} message
   */
  exports.prototype['message'] = undefined;



  return exports;
}));


