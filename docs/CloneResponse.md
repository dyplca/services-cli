# DyplcaServicesApi.CloneResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CloneResponseData**](CloneResponseData.md) |  | [optional] 


