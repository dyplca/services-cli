# DyplcaServicesApi.CloneApi

All URIs are relative to *http://dyplca-services:8080/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clonePost**](CloneApi.md#clonePost) | **POST** /clone | Clone a project


<a name="clonePost"></a>
# **clonePost**
> CloneResponse clonePost(projectId, userId)

Clone a project

Clone a given project and associates it with the given user. 

### Example
```javascript
var DyplcaServicesApi = require('dyplca-services-api');

var apiInstance = new DyplcaServicesApi.CloneApi();

var projectId = "projectId_example"; // String | Id of the project

var userId = "userId_example"; // String | Id of the new owner


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.clonePost(projectId, userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**| Id of the project | 
 **userId** | **String**| Id of the new owner | 

### Return type

[**CloneResponse**](CloneResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

