# DyplcaServicesApi.DownloadApi

All URIs are relative to *http://dyplca-services:8080/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**downloadPost**](DownloadApi.md#downloadPost) | **POST** /download | Download a project


<a name="downloadPost"></a>
# **downloadPost**
> DownloadResponse downloadPost(projectId)

Download a project

Download a given project in excel format. 

### Example
```javascript
var DyplcaServicesApi = require('dyplca-services-api');

var apiInstance = new DyplcaServicesApi.DownloadApi();

var projectId = "projectId_example"; // String | Id of the project


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.downloadPost(projectId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**| Id of the project | 

### Return type

[**DownloadResponse**](DownloadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

