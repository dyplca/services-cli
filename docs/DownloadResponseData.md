# DyplcaServicesApi.DownloadResponseData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** | name of the newly created file available for download. | [optional] 
**absolutePath** | **String** | full absolute path. Might be useless. Depends on local filesystems and sharing properties. | [optional] 


