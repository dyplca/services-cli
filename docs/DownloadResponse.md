# DyplcaServicesApi.DownloadResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**DownloadResponseData**](DownloadResponseData.md) |  | [optional] 


