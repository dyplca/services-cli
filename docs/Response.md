# DyplcaServicesApi.Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **Boolean** | was the requests successfull? | [optional] 
**message** | **String** | iformation about the response weither or not it went well. | [optional] 


