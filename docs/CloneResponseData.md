# DyplcaServicesApi.CloneResponseData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**projectId** | **String** | The Id Of the newly created project | [optional] 
**nbClonedSupplies** | **Number** |  | [optional] 
**nbClonedInterventions** | **Number** |  | [optional] 


