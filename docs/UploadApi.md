# DyplcaServicesApi.UploadApi

All URIs are relative to *http://dyplca-services:8080/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**uploadPost**](UploadApi.md#uploadPost) | **POST** /upload | upload a project


<a name="uploadPost"></a>
# **uploadPost**
> UploadResponse uploadPost(projectId, filename)

upload a project

Upload a given XLSX and update the specified project. 

### Example
```javascript
var DyplcaServicesApi = require('dyplca-services-api');

var apiInstance = new DyplcaServicesApi.UploadApi();

var projectId = "projectId_example"; // String | Id of the project

var filename = "filename_example"; // String | name of the XLS file relative the the working directory


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.uploadPost(projectId, filename, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**| Id of the project | 
 **filename** | **String**| name of the XLS file relative the the working directory | 

### Return type

[**UploadResponse**](UploadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

